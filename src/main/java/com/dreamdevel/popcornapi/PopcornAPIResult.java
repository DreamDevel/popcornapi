package com.dreamdevel.popcornapi;


import org.json.JSONObject;

public interface PopcornAPIResult {

    void onPopcornAPIResult (JSONObject result,Integer methodID);
}
