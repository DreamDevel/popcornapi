package com.dreamdevel.popcornapi;

import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

public class Remote extends AsyncTask<String, Void, JSONObject> {
    private static final String TAG = "Remote";

    private static final Integer RequestError = null;
    private static JSONRPCConnection mJSONRPCConnection;
    private PopcornAPIResult mResultListener;
    private Integer mUniqueMethodID;

    public Remote () {

    }

    public static void setup(String url, String port, String username, String password) {
        mJSONRPCConnection = new JSONRPCConnection();
        mJSONRPCConnection.setup(url,port,username,password);
    }

    private void setPopcornAPIResultListener(PopcornAPIResult resultListener) {
        mResultListener = resultListener;
    }

    // Basic Controls ------------------------------------------------------------------------------
    public void enter () {
        this.execute("enter","");
    }

    public void back () {
        this.execute("back","");
    }

    public void showSettings() {
        this.execute("showsettings","");
    }

    public  void showMovies () {
        this.execute("movieslist","");
    }

    public void showSeries () {
        this.execute("showslist","");
    }

    public void showAnime () {
        this.execute("animelist","");
    }

    public void watchTrailer () {
        this.execute("watchtrailer","");
    }

    // List Actions --------------------------------------------------------------------------------

    public Integer getViewStack(PopcornAPIResult resultListener) {
        setPopcornAPIResultListener(resultListener);
        this.execute("getviewstack","");
        return generateUniqueMethodID();
    }

    public Integer getCurrentList(int page,PopcornAPIResult resultListener) {
        try {
            setPopcornAPIResultListener(resultListener);
            JSONArray params = new JSONArray(new int[]{page});
            this.execute("getcurrentlist",params.toString());

            return generateUniqueMethodID();
        } catch (Exception ex) {
            Log.w(TAG,"Couldn't create parameters array",ex);
        }

        return RequestError;
    }

    public Integer ping (PopcornAPIResult resultListener) {
        setPopcornAPIResultListener(resultListener);
        this.execute("ping","");
        return generateUniqueMethodID();
    }

    public Integer getSubtitles (PopcornAPIResult resultListener) {
        setPopcornAPIResultListener(resultListener);
        this.execute("getsubtitles","");
        return generateUniqueMethodID();
    }

    public Integer getPlaying(PopcornAPIResult resultListener) {
        setPopcornAPIResultListener(resultListener);
        this.execute("getplaying","");
        return generateUniqueMethodID();
    }

    public Integer getLoading(PopcornAPIResult resultListener) {
        setPopcornAPIResultListener(resultListener);
        this.execute("getloading","");
        return generateUniqueMethodID();
    }

    public Integer getSelection (PopcornAPIResult resultListener) {
        setPopcornAPIResultListener(resultListener);
        this.execute("getselection","");
        return generateUniqueMethodID();
    }

    public Integer getNotifications(PopcornAPIResult resultListener) {
        setPopcornAPIResultListener(resultListener);
        this.execute("listennotifications","");
        return generateUniqueMethodID();
    }

    public void setSelection (int index) {
        try {
            JSONArray params = new JSONArray(new int[]{index});
            this.execute("setselection",params.toString());
        } catch (Exception ex) {
            Log.w(TAG,"Couldn't create parameters array",ex);
        }
    }

    public void togglePlaying () {
        this.execute("toggleplaying","");
    }

    public void seek(int seek) {
        try {
            JSONArray params = new JSONArray(new int[]{seek});
            this.execute("seek",params.toString());
        } catch (Exception ex) {
            Log.w(TAG,"Couldn't create parameters array",ex);
        }
    }

    @Override
    protected JSONObject doInBackground(String... input) {
        Log.d(TAG,"executing " + input[0]);
        String method = input[0];
        String params = input[1];

        if (mResultListener != null) {
            JSONObject result = mJSONRPCConnection.call(method,params);
            return result;
        } else {
            mJSONRPCConnection.call(method,params);
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        if (mResultListener != null)
            mResultListener.onPopcornAPIResult(result,mUniqueMethodID);

    }

    private Integer generateUniqueMethodID(){

        mUniqueMethodID = (int) (Math.random()*(Integer.MAX_VALUE - Integer.MIN_VALUE)) +
                Integer.MIN_VALUE;

        return mUniqueMethodID;
    }
}
