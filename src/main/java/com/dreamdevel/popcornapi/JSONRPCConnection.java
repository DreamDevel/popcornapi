package com.dreamdevel.popcornapi;

import android.util.Log;
import java.net.*;
import org.apache.commons.codec.binary.Base64;
import java.io.*;
import org.json.*;

public class JSONRPCConnection {
    private static final String TAG = "JSONRPCConnection";

    private String mUsername;
    private String mPassword;
    private String mUrl;
    private String mPort;
    private HttpURLConnection mConnection;



    public void setup(String url, String port,String username,String password) {
        mUsername = username;
        mPassword = password;
        mUrl = url;
        mPort = port;
    }

    public JSONObject call(String method, String params) {
        try {
            createConnection();
            if(method.equals("ping"))
                setConnectionTimeout(500);
            String content = createContent(method,params);
            sendRequest(content);
            return getResponse();
        } catch (Exception ex) {
            if (ex.getClass() == SocketTimeoutException.class && method.equals("ping")) {
                Log.w(TAG,mUrl + " did not responde from ping");
            } else {
                Log.w(TAG,"Couldn't Make RPC Call",ex);
            }
        }

        return null;
    }

    private void createConnection () throws Exception {
        String userCredentials = mUsername + ":" + mPassword;
        String basicAuth = "Basic " + new String(Base64.encodeBase64(userCredentials.getBytes()));
        URL url = new URL(mUrl + ":" + mPort);


        mConnection = (HttpURLConnection) url.openConnection();
        mConnection.setRequestMethod("POST");
        mConnection.setRequestProperty("Content-Type","application/json");
        mConnection.setRequestProperty("Authorization",basicAuth);

        mConnection.setUseCaches(false);
        mConnection.setDoOutput(true);

    }

    private void setConnectionTimeout(int ms) {
        mConnection.setConnectTimeout(ms);
    }

    private String createContent(String method, String params) throws org.json.JSONException {
        String content = createJSONRPCCall(method,params);
        // Set Content length to http headers now that we have it
        mConnection.setRequestProperty("Content-Length",Integer.toString(content.getBytes().length));
        return content;
    }

    private String createJSONRPCCall (String method,String params) throws org.json.JSONException {
        // Parameters should not be empty
        String finalParams = params;
        if (params.length() == 0)
            finalParams ="[]";

        return  "{\"jsonrpc\": \"2.0\", \"method\": \"" + method + "\", \"params\": " + finalParams + ", \"id\": 1}";
    }

    private void sendRequest(String content) throws Exception {
        DataOutputStream wr = new DataOutputStream (mConnection.getOutputStream());
        wr.writeBytes(content);
        wr.close();
    }

    private JSONObject getResponse() throws Exception {
    // Print Header Response
    /*
    Map<String, List<String>> map = connection.getHeaderFields();
    System.out.println("Printing Response Header...\n");
    for (Map.Entry<String, List<String>> entry : map.entrySet()) {
      System.out.println(entry.getKey() + " : " + entry.getValue());
    }*/

        InputStream is = mConnection.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        StringBuffer response = new StringBuffer();
        String line;
        while ((line = rd.readLine()) != null) {
            response.append(line);
        }
        rd.close();

        // Convert Response to Object
        JSONObject responseObject = new JSONObject(response.toString());
        return responseObject;
    }

    public void disconnect() {
        if (mConnection != null) {
            mConnection.disconnect();
        }
    }

}
