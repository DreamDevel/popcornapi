package com.dreamdevel.popcornapi;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class ItemCollection implements Serializable {
    private static final String TAG = "ItemCollection";

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public ArrayList<Item> getList() {
        return list;
    }

    public void setList(ArrayList<Item> list) {
        this.list = list;
    }

    private String mType;
    private ArrayList<Item> list;

    public ItemCollection() {

    }

    public ItemCollection (JSONObject itemCollectionObject) {
        parseJSONObject(itemCollectionObject);
    }

    public void parseJSONObject (JSONObject itemCollectionObject) {
        try {
            setType(itemCollectionObject.getString("type"));

            list = new ArrayList<Item>();
            JSONArray list = itemCollectionObject.getJSONArray("list");
            for( int i = 0; i < list.length(); i++) {
                JSONObject object = list.getJSONObject(i);
                Item item = new Item(object);
                getList().add(item);
            }
        }catch (Exception ex) {
            Log.w(TAG,"Couldn't parse Item Collection JSON Object",ex);
        }
    }
}
