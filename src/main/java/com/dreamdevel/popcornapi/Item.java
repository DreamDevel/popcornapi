package com.dreamdevel.popcornapi;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class Item implements Serializable {
    private static final String TAG = "Item";

    private String mType;
    private String mImdb_id;
    private String mRuntime;
    private String mTrailer;
    private String mSynopsis;
    private double mRating;
    private String mYear;
    private String mCover;
    private String mTitle;
    private ArrayList<String> mGenre;
    private HashMap<String,TorrentInfo> mTorrents;



    public Item() {

    }

    public Item(JSONObject item) {
        parseJSONItem(item);
    }

    public void parseJSONItem(JSONObject item) {
        try {
            // TODO add all object's fields
            setTitle(item.getString("title"));
            setType(item.getString("type"));
            setCover(item.getString("cover"));
            setYear(item.getString("year"));
            setRating(item.getDouble("rating"));
            setSynopsis(item.getString("synopsis"));
            setRuntime(item.getString("runtime"));
            setTrailer(item.getString("trailer"));

            ArrayList<String> genre = new ArrayList<>();
            JSONArray genreJSONArray = item.getJSONArray("genre");
            for (int i = 0; i < genreJSONArray.length(); i++) {
                genre.add(genreJSONArray.getString(i));
            }

            setGenre(genre);

            HashMap<String,TorrentInfo> torrents = new HashMap<>();
            JSONObject torrentsObject = item.getJSONObject("torrents");
            if(torrentsObject.has("720p")) {
                TorrentInfo torrent720p = new TorrentInfo(torrentsObject.getJSONObject("720p"),"720p");
                torrents.put("720p",torrent720p);
            }

            if(torrentsObject.has("1080p")) {
                TorrentInfo torrent1080p = new TorrentInfo(torrentsObject.getJSONObject("1080p"), "1080p");
                torrents.put("1080p",torrent1080p);
            }

            setTorrents(torrents);
        } catch (Exception ex) {
            Log.w(TAG,"Couldn't parse JSON Item",ex);
        }
    }

    public String getTrailer() {
        return mTrailer;
    }

    public void setTrailer(String trailer) {
        mTrailer = trailer;
    }

    public String getRuntime() {
        return mRuntime;
    }

    public void setRuntime(String runtime) {
        mRuntime = runtime;
    }

    public String getSynopsis() {
        return mSynopsis;
    }

    public void setSynopsis(String synopsis) {
        mSynopsis = synopsis;
    }

    public double getRating() {
        return mRating;
    }

    public void setRating(double rating) {
        mRating = rating;
    }

    public String getYear() {
        return mYear;
    }

    public void setYear(String year) {
        mYear = year;
    }

    public String getCover() {
        return mCover;
    }

    public void setCover(String cover) {
        mCover = cover;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getImdb_id() {
        return mImdb_id;
    }

    public void setImdb_id(String imdb_id) {
        mImdb_id = imdb_id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public ArrayList<String> getGenre() {
        return mGenre;
    }

    public void setGenre(ArrayList<String> genre) {
        mGenre = genre;
    }

    public HashMap<String, TorrentInfo> getTorrents() {
        return mTorrents;
    }

    public void setTorrents(HashMap<String, TorrentInfo> torrents) {
        mTorrents = torrents;
    }
}
