package com.dreamdevel.popcornapi;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class TorrentInfo implements Serializable {
    private static final String TAG = "TorrentInfo";

    private String mProvider;
    private String mFileSize;
    private Integer mSize; // Bytes
    private Integer mPeer;
    private Integer mSeed;
    private String mQuality;
    private String mUrl;
    private String mHealth;

    public TorrentInfo() {
    }

    public TorrentInfo(JSONObject item,String quality) {
        parseJSONItem(item);
        setQuality(quality);
    }

    public void parseJSONItem(JSONObject item) {
        try {
            setProvider(item.getString("provider"));
            setFileSize(item.getString("filesize"));
            setSize(item.getInt("size"));
            setSeed(item.getInt("seed"));
            setPeer(item.getInt("peer"));
            setUrl(item.getString("url"));
            if(item.has("health"))
                setHealth(item.getString("health"));
            else
                setHealth("");

        } catch (Exception ex) {
            Log.w(TAG, "Couldn't parse JSON Item", ex);
        }
    }


    public String getProvider() {
        return mProvider;
    }

    public void setProvider(String provider) {
        mProvider = provider;
    }

    public String getFileSize() {
        return mFileSize;
    }

    public void setFileSize(String fileSize) {
        mFileSize = fileSize;
    }

    public Integer getSize() {
        return mSize;
    }

    public void setSize(Integer size) {
        mSize = size;
    }

    public Integer getPeer() {
        return mPeer;
    }

    public void setPeer(Integer peer) {
        mPeer = peer;
    }

    public Integer getSeed() {
        return mSeed;
    }

    public void setSeed(Integer seed) {
        mSeed = seed;
    }

    public String getQuality() {
        return mQuality;
    }

    public void setQuality(String quality) {
        mQuality = quality;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getHealth() {
        return mHealth;
    }

    public void setHealth(String health) {
        this.mHealth = health;
    }
}
